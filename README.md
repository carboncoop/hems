Carbon Co-op Home Energy Management System
==========================================

This is the main repository for the HEMS software deployment. This currently
consists of

* A Home Assistant build with custom components:
  * Custom base configuration (homeassisstant/config/configuration.yml).
  * Custom automations (homeassistant/config/automations.yml).
  * Custom groups (homeassistant/config/groups.yaml).
  * Auto-discovery and configuration scripts for shelly and emonevse (homeassistant/config/python_scripts/*).
  * PowerShaper custom integration (for connecting to DRMS/VTN) (homeassisstant/custom_components/powershaper).
* The HEMS supervisor; used for applying network settings at runtime (supervisor/).
* A telegraf client used for telemetry (telemetry/).
* Docker compose file examples for use of above container applications (./docker) including configuration of an MQTT broker for development. These are mainly used to copy configuration into containers as balena doesn't support mounting data into containers.
* Balena compose files used for deployment using balena-push (balena/).
* Gitlab CI/CD file (.gitlab-ci.yml).
* HEMS provisioning scripts (provisioning/*). These are used to create HEMS for deployment in the field.
* Documentation for hardware testing and installation can be found in the main OpenDSR docs.

Build and Deploy
================
There are several options for development which suit different workflows and depend on the task at hand.

Local development on laptop (run directly on your laptop)
---------------------------------------------------------------------------------------
To run a local version on your laptop you need to make an env file (docker/local.env) containing dummy UUID and Balena name values. See docker/example.env for format.
```
docker-compose -f ./docker/local.yml up --build
```
Run a local version on laptop connecting to the MQTT broker on a HEMS:
```
docker-compose -f ./docker/local-no-mqtt.yml up --build
```
Note that certain environment variables usually present in a Balena runtime environment must be set in the local.env file referenced in the compose file. If you want to connect to a VTN instance also running on localhost you will need to add a ```network_mode: host``` directive to both the homeassistant and mosquitto services in your local yml.

Balena local build & deploy to a local mode dev device (e.g. RPI3 on your desk)
-----------------------------------------------------------------------
This builds on a local device without going via cloud service. It is important to
install the standalone version of the balena-cli as the NPM package has bugs which
prevent local push from working.

balena push can only use compose files located in the root of the repository so
first you will need to copy the relevant compose file from the balena/ folder e.g.
```
cp <project root>/balena/balena-staging.yml <project root>/docker-compose.yml
```

Remove the `image` tag references in the docker-compose.yml file as these interfere with the balena local deployment.

Then you can do a local push using:
```
balena push $DEVICE_IP_ADDRESS`
```

Balena remote build & deploy (e.g. will stage for OTA update to remote devices)
--------------------------------------------------------
e.g.
```
balena login
balena push carboncoop-hems-staging
```

Pipeline build
--------------
A commit to the dev/master branch will trigger a pipeline build. In both cases an SD card image is produced containing the latest application version and archived on S3 for use in provisioning.

Pushes to the 'dev' branch will goto all staged test setups. Pushes to 'master' will goto canary installations not the whole device fleet.

Pipeline builds take about 5 minutes to be deployed to devices (first they are built on gitlab runners, then the image is pushed to Balena, then they are dled to Balena devices, then the containers are restarted).
