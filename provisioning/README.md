HEMS Provisioning
=================

Requirements
------------
* Install Balena CLI (IMPORTANT: use the latest standalone version from github)
* Install AWS S3 CLI (either package manager or AWS direct DL)
* Install jq (probably package manager)

Prerequisites
-------------
* A Balena Cloud account connected with the appopriate application.
* An AWS account with permissions to access the S3 bucket containing the images.

Instructions
------------

This folder contains the files and scripts used in HEMS provisioning which implement the provisioning flow.

If an image exists already you can start at 2.
```
#. Login to balena-cli with `balena login` (if required)
#. Run 1_download_sdimg.sh to download latest image (this only needs to be done once in any session).
#. Run (with sudo) 2_prepare_sdimg.sh to create a new device on balena and generate an image for this device. The image will be named using the balena name e.g. happy-lion.img.
#. Copy and modify the command found in 3_write_sdcard.sh to use balena local flash to burn the image to an SD card.
#. 4_cleanup.sh will clean up temporary files. This can be run to reset the session.
#. Device data is appended to devices.log (for making labels etc.).
```

Raspberry Pi 3/4 and 32/64 bit
----------------
To select a different architecture edit `ARCH_NAME` in the `1_download_sdimg.sh` script (default is RPI3-32bit).

Possible choices are:
* rapsberrypi3
* rapsberrypi3-64
* rapsberrypi4-64
