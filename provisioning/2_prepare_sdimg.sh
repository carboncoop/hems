#!/bin/bash
# You need to install the following dependencies:
#  * balena-cli (https://github.com/balena-io/balena-cli/blob/master/INSTALL.md)

# If temporary image folder does not exist create it 
mkdir -p tmp

# Import env variables generated in last step
. create_env.sh

# Generate UUID for new Balena device
BALENA_UUID=`uuidgen|tr -d '-'`
BALENA_UUID_SHORT=${BALENA_UUID:0:4}

# Register (create) new Balena device
balena device register carboncoop-hems-$RELEASE --uuid $BALENA_UUID

# Extract balena device short name
BALENA_NAME=`balena device $BALENA_UUID|head -n 1|cut -c4-|tr ' ' '-'|tr A-Z a-z`

# Make copy of base image for provisioning
cp hems-sdcard.img tmp/$BALENA_NAME.img

#** This line is no longer required but has been retained to show alternative way of configuring - its disadvantage is that the BalenaOS version needs to be determined from the image or otherwise
balena config generate --device $BALENA_UUID --version $BALENA_OS_VERSION --network ethernet --appUpdatePollInterval 10 -o config.json

# Provision the disk image with a config.json
balena os configure ./tmp/$BALENA_NAME.img --device $BALENA_UUID --config config.json

# Set the hostname
balena config write -t raspberrypi3 -d ./tmp/$BALENA_NAME.img hostname ccoop-hems-$BALENA_UUID_SHORT

zip -r ./tmp/$BALENA_NAME.img.zip ./tmp/$BALENA_NAME.img
rm ./tmp/$BALENA_NAME.img

# Copy the image/device information to a file for use in labelling etc.
if [ -f "./tmp/$BALENA_NAME.img.zip" ]; then
    echo $BALENA_UUID","$BALENA_NAME","`date -Iseconds`",ccoop-hems-"${BALENA_UUID:0:4} >>./devices.log
fi
