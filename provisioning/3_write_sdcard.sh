#This is an example of how to use dd to copy a single disk image. You should carefully indentify the block device to use and insert this
#in place of /dev/yourdisk
sudo balena local flash ./tmp/$BALENA_NAME.img.zip --drive /dev/yourdisk
