"""The sentry integration."""
import os
import re
from typing import Dict, Union

import sentry_sdk
import voluptuous as vol
from sentry_sdk.integrations.aiohttp import AioHttpIntegration
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration

from homeassistant.const import EVENT_HOMEASSISTANT_STARTED
from homeassistant.const import __version__ as current_version
from homeassistant.core import HomeAssistant
from homeassistant.helpers import config_validation as cv
from homeassistant.helpers import entity_platform
from homeassistant.loader import Integration, async_get_custom_components

from . import const

CONFIG_SCHEMA = vol.Schema(
    {
        const.DOMAIN: vol.Schema(
            {
                vol.Required(const.CONF_DSN): cv.url,
            }
        )
    },
    extra=vol.ALLOW_EXTRA,
)

LOGGER_INFO_REGEX = re.compile(r"^(\w+)\.?(\w+)?\.?(\w+)?\.?(\w+)?(?:\..*)?$")


async def async_setup(hass: HomeAssistant, config: dict) -> bool:
    """Set up the Sentry component."""
    # Additional/extra data collection
    huuid = await hass.helpers.instance_id.async_get()
    system_info = await hass.helpers.system_info.async_get_system_info()
    custom_components = await async_get_custom_components(hass)
    config = config.get(const.DOMAIN)
    sentry_sdk.init(
        dsn=config.get(const.CONF_DSN),
        environment=os.environ.get("BALENA_APP_NAME"),
        integrations=[AioHttpIntegration(), SqlalchemyIntegration()],
        traces_sample_rate=0.2,
        release=current_version,
        before_send=lambda event, hint: process_before_send(
            hass,
            huuid,
            system_info,
            custom_components,
            event,
            hint,
        ),
    )

    async def update_system_info(now):
        nonlocal system_info
        system_info = await hass.helpers.system_info.async_get_system_info()

        # Update system info every hour
        hass.helpers.event.async_call_later(3600, update_system_info)

    hass.bus.async_listen_once(EVENT_HOMEASSISTANT_STARTED, update_system_info)
    return True


def process_before_send(
    hass: HomeAssistant,
    huuid: str,
    system_info: Dict[str, Union[bool, str]],
    custom_components: Dict[str, Integration],
    event,
    hint,
):
    """Process a Sentry event before sending it to Sentry."""
    # Additional tags to add to the event
    additional_tags = {
        "device_name": os.environ.get("BALENA_DEVICE_NAME_AT_INIT"),
        "installation_type": system_info["installation_type"],
        "uuid": huuid,
    }

    # Find out all integrations in use, filter "auth", because it
    # triggers security rules, hiding all data.
    integrations = [
        integration
        for integration in hass.config.components
        if integration != "auth" and "." not in integration
    ]

    # Add additional tags based on what caused the event.
    platform = entity_platform.current_platform.get()
    if platform is not None:
        # This event happened in a platform
        additional_tags["custom_component"] = "no"
        additional_tags["integration"] = platform.platform_name
        additional_tags["platform"] = platform.domain
    elif "logger" in event:
        # Logger event, try to get integration information from the logger name.
        matches = LOGGER_INFO_REGEX.findall(event["logger"])
        if matches:
            group1, group2, group3, group4 = matches[0]
            # Handle the "homeassistant." package differently
            if group1 == "homeassistant" and group2 and group3:
                if group2 == "components":
                    # This logger is from a component
                    additional_tags["custom_component"] = "no"
                    additional_tags["integration"] = group3
                    if group4 and group4 in const.ENTITY_COMPONENTS:
                        additional_tags["platform"] = group4
                else:
                    # Not a component, could be helper, or something else.
                    additional_tags[group2] = group3
            else:
                # Not the "homeassistant" package, this third-party
                additional_tags["package"] = group1

    # If this event is caused by an integration, add a tag if this
    # integration is custom or not.
    if (
        "integration" in additional_tags
        and additional_tags["integration"] in custom_components
    ):
        additional_tags["custom_component"] = "yes"

    # Update event with the additional tags
    event.setdefault("tags", {}).update(additional_tags)

    # Set user context to the installation UUID
    event.setdefault("user", {}).update({"id": huuid})

    # Update event data with Home Assistant Context
    event.setdefault("contexts", {}).update(
        {
            "Home Assistant": {
                "custom_components": "\n".join(sorted(custom_components)),
                "integrations": "\n".join(sorted(integrations)),
                **system_info,
            },
        }
    )
    return event
