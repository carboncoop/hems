"""Constants for the Outgoing Octopus integration."""

DOMAIN = "outgoing_octopus"

REGION_CODE = "region_code"
