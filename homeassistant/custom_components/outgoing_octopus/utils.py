"""Small utility functions."""

from homeassistant.helpers.typing import HomeAssistantType

from .const import DOMAIN, REGION_CODE


def get_region_code(hass: HomeAssistantType) -> str:
    """Return the Octopus region code."""
    region_code = hass.config_entries.async_entries(DOMAIN)[0].data[REGION_CODE]

    return region_code
