class DynamicElectrictyRatesCard extends HTMLElement {
  constructor() {
    super();
    this.config = {};
  }

  set hass(hass) {
    if (!this.content) {
      const card = document.createElement("ha-card");
      card.header = this.config.title;
      this.content = document.createElement("div");
      this.content.style.padding = "0 16px 16px";

      const style = document.createElement("style");
      style.textContent = `
            table {
                width: 100%;
                padding: 0px;
                spacing: 0px;
            }
            table.sub_table {
                border-collapse: separate;
                border-spacing: 0px 2px;
            }
            table.main {
                padding: 0px;
            }
            thead th {
                text-align: left;
                padding: 0px;
            }
            td {
                vertical-align: top;
                padding: 2px;
                spacing: 0px;
            }
            tr.rate_row{
                text-align:center;
                width:80px;
            }
            td.time {
                text-align:center;
                vertical-align: middle;
            }
            td.time_red{
                border-bottom: 1px solid Tomato;
            }
            td.time_orange{
                border-bottom: 1px solid orange;
            }
            td.time_green{
                border-bottom: 1px solid MediumSeaGreen;
            }
            td.time_blue{
                border-bottom: 1px solid #391CD9;
            }
            td.rate {
                color:white;
                text-align:center;
                vertical-align: middle;
                width:80px;

                border-top-right-radius:15px;
                border-bottom-right-radius:15px;
            }
            td.red {
                border: 2px solid Tomato;
                background-color: Tomato;
            }
            td.orange {
                border: 2px solid orange;
                background-color: orange;
            }
            td.green {
                border: 2px solid MediumSeaGreen;
                background-color: MediumSeaGreen;
            }
            td.blue {
                border: 2px solid #391CD9;
                background-color: #391CD9;
            }
            `;
      card.appendChild(style);
      card.appendChild(this.content);
      this.appendChild(card);
    }

    const entityId = this.config.entity;
    const state = hass.states[entityId];
    console.debug("Printing rates for " + entityId);
    if (!state) {
      console.error(
        "Error whie setting up rates card, entity " + entityId + " is not defined; is the integration installed?"
      );

      this.content.innerHTML = "<hui-warning>Entity <em>" + entityId + "</em> is not defined; is the integration installed?</hui-warning>";

      // If we throw an error here the card won't render and we can never change the entity or even delete the card!
      return;
    }

    const attributes = this._reverseObject(state.attributes);
    let tables = "<td><table class='sub_table'><tbody>";
    const ratesListLength = Object.keys(attributes).length;
    const rowsPerCol = Math.ceil(ratesListLength / this.config.cols);

    let table = "";
    let x = 1;
    const unitStr = this.config.unitstr;
    const roundUnits = this.config.roundUnits;

    const lang = navigator.language || navigator.languages[0];

    for (let key of Object.keys(attributes)) {
      const dateMilli = Date.parse(key);
      let date = new Date(dateMilli);
      let options = { hour12: false, hour: "2-digit", minute: "2-digit" };
      let timeLocale = date.toLocaleTimeString(lang, options);
      let colour = this._getColourForValue(attributes[key]);
      table = table.concat(`<tr class='rate_row'>
            <td class='time time_${colour}'>${timeLocale}</td>
            <td class='rate ${colour}'>${attributes[key].toFixed(
        roundUnits
      )}${unitStr}</td>
            </tr>`);

      if (x % rowsPerCol == 0) {
        tables = tables.concat(table);
        table = "";
        if (ratesListLength != x) {
          tables = tables.concat("</tbody></table></td>");
          tables = tables.concat("<td><table class='sub_table'><tbody>");
        }
      }
      x++;
    }

    tables = tables.concat(table);
    tables = tables.concat("</tbody></table></td>");

    this.content.innerHTML = `
        <table class="main">
            <tr>
                ${tables}
            </tr>
        </table>
        `;
  }

  setConfig(inputConfig) {
    if (!inputConfig.entity) {
      throw new Error("You need to define an entity");
    }

    const defaultConfig = DynamicElectrictyRatesCard.getStubConfig();

    this.config = Object.assign(defaultConfig, inputConfig);
  }

  /**
   * The height of your card. Home Assistant uses this to automatically
   * distribute all cards over the available columns.
   */
  getCardSize() {
    // TODO: Check if this should be hardcoded or do something more specific
    return 3;
  }

  static async getConfigElement() {
    return document.createElement("dynamic-electricity-rates-card-editor");
  }

  /**
   * Returns the default settings Lovelace will use.
   */
  static getStubConfig() {
    const defaultConfig = {
      title: "Electricity Rates",
      entity: "octopusagile.rates",
      cols: 3,
      mediumlimit: 10,
      highlimit: 15,
      roundUnits: 2,
      showunits: "true",
      outgoing: false,

      get unitstr() {
        return this.showunits === "true" ? "p/kWh" : "";
      },
      set unitstr(input) {}, // HA will try to set this.

    };
    return defaultConfig;
  }

  _getColourForValue(value) {
    let colourMap;
    if (this.config.outgoing === "true") {
      colourMap = {
        high: "green",
        medium: "orange",
        low: "red",
        negative: "darkred",
      };
    } else {
      colourMap = {
        high: "red",
        medium: "orange",
        low: "green",
        negative: "blue",
      };
    }

    let colour = colourMap["low"];
    if (value > this.config.highlimit) {
      colour = colourMap["high"];
    } else if (value > this.config.mediumlimit) {
      colour = colourMap["medium"];
    } else if (value <= 0) colour = colourMap["negative"];
    return colour;
  }

  _reverseObject(object) {
    let newObject = {};
    let keys = [];

    for (let key in object) {
      keys.push(key);
    }

    for (let i = keys.length - 1; i >= 0; i--) {
      let value = object[keys[i]];
      newObject[keys[i]] = value;
    }

    return newObject;
  }
}

/**
 * A placeholder HTML element for our card's configuration settings- currently blank.
 */
class DynamicElectrictyRatesCardEditor extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.config = {};
    this.initialised = false;
  }

  setConfig(config) {
    if (!config.entity) {
      throw new Error("You need to define an entity");
    }

    this.config = config;

    this.lovelace = {};
    if (!this.initialised) {
      const outerDiv = document.createElement("div");
      outerDiv.classList.add("card-config");
      outerDiv.innerHTML = `<style>ha-formfield {
                margin-top: 1em;
                display: flex;
            }</style>`;

      const card = this;

      // TODO: fix the entity picker.
      // const entityPicker = document.createElement('ha-entity-picker');
      // Object.assign(entityPicker, {
      //     label: "Entity",
      //     name: "entity",
      //     required: true,
      //     value: this.config.entity,
      //     onchange: this.updateSetting.bind(card)
      // });
      // outerDiv.append(entityPicker);

      const titleInput = document.createElement("paper-input");
      Object.assign(titleInput, {
        label: "Card Title",
        name: "title",
        required: true,
        value: this.config.title,
        onchange: this.updateSetting.bind(card),
      });
      outerDiv.append(titleInput);

      const colInput = document.createElement("paper-input");
      Object.assign(colInput, {
        label: "Columns",
        name: "cols",
        type: "number",
        min: 1,
        value: this.config.cols,
        onchange: this.updateSetting.bind(card),
      });
      outerDiv.append(colInput);

      const highLimitInput = document.createElement("paper-input");
      Object.assign(highLimitInput, {
        label: "High limit",
        name: "highlimit",
        type: "number",
        value: this.config.highlimit,
        onchange: this.updateSetting.bind(card),
      });
      outerDiv.append(highLimitInput);

      const medLimitInput = document.createElement("paper-input");
      Object.assign(medLimitInput, {
        label: "Medium limit",
        name: "mediumlimit",
        type: "number",
        value: this.config.mediumlimit,
        onchange: this.updateSetting.bind(card),
      });
      outerDiv.append(medLimitInput);

      this.shadowRoot.append(outerDiv);

      const roundToInput = document.createElement("paper-input");
      Object.assign(roundToInput, {
        label: "Round units",
        name: "roundUnits",
        type: "number",
        min: 0,
        value: this.config.roundUnits,
        onchange: this.updateSetting.bind(card),
      });
      outerDiv.append(roundToInput);

      const showUnitsWrapper = document.createElement("ha-formfield");
      Object.assign(showUnitsWrapper, {
        label: "Show Units?",
      });
      outerDiv.append(showUnitsWrapper);

      const showUnitsInput = document.createElement("ha-switch");
      Object.assign(showUnitsInput, {
        label: "Show units",
        name: "showunits",
        type: "checkbox",
        checked: this.config.showunits === "true",
        onchange: this.updateSetting.bind(card),
      });
      showUnitsWrapper.append(showUnitsInput);

      const outgoingWrapper = document.createElement("ha-formfield");
      Object.assign(outgoingWrapper, {
        label: "Outgoing power (high prices are better)",
      });
      outerDiv.append(outgoingWrapper);
      const outgoingInput = document.createElement("ha-switch");

      Object.assign(outgoingInput, {
        label: "Outgoing power (high prices are better)",
        name: "outgoing",
        type: "checkbox",
        checked: this.config.outgoing === "true",
        onchange: this.updateSetting.bind(card),
      });
      outgoingWrapper.append(outgoingInput);

      this.shadowRoot.append(outerDiv);
      this.initialised = true;
    }
  }

  set hass(hassInput) {
    if ("outgoing_octopus" in hassInput.services) {
      this.outgoingInstalled = true;
    } else {
      this.outgoingInstalled = false;
    }
  }

  /**
   * Process and submit settings changes to HASS.
   */
  updateSetting(changeEvent) {
    const name = changeEvent.target.name;

    const configEvent = new Event("config-changed", {
      bubbles: true,
      composed: true,
    });

    let newConfig = Object.assign({}, this.config);
    if (changeEvent.target.type === "checkbox") {
      newConfig[name] = changeEvent.target.checked ? "true" : "false";

      if (name === "outgoing") {
        newConfig.entity = changeEvent.target.checked ? "outgoing_octopus.rates" : "octopusagile.rates";
    }
    } else {
      newConfig[name] = changeEvent.target.value;
    }
    configEvent.detail = { config: newConfig };
    this.dispatchEvent(configEvent);
  }
}

customElements.define(
  "dynamic-electricity-rates-card",
  DynamicElectrictyRatesCard
);
customElements.define(
  "dynamic-electricity-rates-card-editor",
  DynamicElectrictyRatesCardEditor
);

// Add the card to Lovelace's card list so it shows up as an option.
window.customCards = window.customCards || [];
window.customCards.push({
  type: "dynamic-electricity-rates-card",
  name: "Octopus Agile Rates Card",
  description:
    "Octopus Agile custom card to display future rates in Home Assistant.",
  entity: "octopusagile.rates",
});
