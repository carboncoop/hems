#!/bin/bash

FILE=/carboncoop-hems-homeassistant-data/ha.db

if test -f "$FILE" ; then
    sqlite3 $FILE "VACUUM;"
fi
touch /config/.storage/automations.yaml
python3 -m homeassistant --config /config --debug



